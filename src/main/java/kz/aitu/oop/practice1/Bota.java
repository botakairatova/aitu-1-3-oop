package kz.aitu.oop.practice1;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class Bota {
    public static boolean fileExists(){
        try {
            File file = new File("Desktop://:ICT://Kairatova Akbota lab3");
            FileReader fr = new FileReader(file);
            return true;
        } catch (FileNotFoundException e) {
            System.out.println("File does not exist");
            return false;
        }
        finally {
            System.out.println("We are in final");
        }
    }
    public boolean isInt(String s){
        try{
            Integer.parseInt(s);
            System.out.println(s);
            return true;
        }
        catch (NumberFormatException e){
            return false;
        }
        finally {
            System.out.println("We are in final");
        }
    }
    public boolean isDouble(String s){
         try{
             Double.parseDouble(s);
             System.out.println(s);
             return true;
         }
         catch (NumberFormatException e){
             return false;
         }
         finally {
             System.out.println("We are in final");
         }
    }
}
