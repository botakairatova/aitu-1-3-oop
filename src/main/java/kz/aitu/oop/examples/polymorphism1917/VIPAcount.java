package kz.aitu.oop.examples.polymorphism1917;

public class VIPAcount extends Account {


    private double cashBack;
    private double bonus;

    public VIPAcount(String creditCard, double balance, double cashBack) {
        super(creditCard, balance);
        if(cashBack > 0) this.cashBack = cashBack;
        bonus = 0;
    }

    public void removeMoney(double money) {
        super.removeMoney(money);
        if(money > 0) bonus += money * cashBack / 100;
    }

    public double getBonus() {
        return bonus;
    }
}
