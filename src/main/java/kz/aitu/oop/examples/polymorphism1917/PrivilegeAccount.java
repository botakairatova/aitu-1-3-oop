package kz.aitu.oop.examples.polymorphism1917;

public class PrivilegeAccount extends Account {

    private double discount;

    public PrivilegeAccount(String creditCard, double balance, double discount) {
        super(creditCard, balance);

        if(discount > 0 || discount <= 100) this.discount = discount;
    }

    public void removeMoney(double money) {
        if(money <= 0) return;
        money = money * (100-discount)/100;
        super.removeMoney(money);
    }

    public double getDiscount() {
        return discount;
    }
}
