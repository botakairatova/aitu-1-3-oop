package kz.aitu.oop.examples.practice5;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class Precious extends Stone {

    public Precious(String name, String type, int weight, int price) {
        super(name, type, weight, price);
    }

    @Override
    public void toStrinng() {
        try {
            Connection conn = DriverManager.getConnection(url, username, password);
            Statement statement = ((Connection) conn).createStatement();
            ResultSet result = statement.executeQuery("select * from stone where type = 'precious'");
            while(result.next()) {
                System.out.println(result.getString("type") + " stone "
                        + result.getString("name") + " with price: "
                        + result.getString("price") + " and weight: "
                        + result.getString("weight"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
