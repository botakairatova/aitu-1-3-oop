package kz.aitu.oop.examples.practice5;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class Stone {
    public String name;
    public String type;
    public int weight;
    public int price;
    public ArrayList<String> neclace;

    public Stone(String name, String type, int weight, int price) {
        this.name = name;
        this.type = type;
        this.weight = weight;
        this.price = price;
    }

    String url = "jdbc:mysql://localhost:3306/bota";
    String username = "root";
    String password = "Botakan2001";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public ArrayList<String> getNeclace() {
        return neclace;
    }

    public void setNeclace(ArrayList<String> neclace) {
        this.neclace = neclace;
    }

    public static class StoneBuilder{
        public String name;
        public String type;
        public int weight;
        public int price;

        public StoneBuilder(String type){
            this.type = type;
        }

        public StoneBuilder withName(String name){
            this.name = name;
            return this;
        }

        public StoneBuilder withWeight(int weight){
            this.weight = weight;
            return this;
        }

        public StoneBuilder withPrice(int price){
            this.price = price;
            return this;
        }

        public Stone build(){
            return new Stone(name,type,weight,price);
        }
    }


    public void totalCostPrecoius(){
        try {
            Connection conn = DriverManager.getConnection(url, username, password);
            Statement statement = ((Connection) conn).createStatement();
            ResultSet result = statement.executeQuery("select * from stone where type = 'precious'");
            int cost = 0;
            while(result.next()){
                cost = cost +result.getInt("price");
            }
            System.out.println(result.getInt("Total cost is " + cost + "$"));
            System.out.println("------------------------------");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void totalCostSemiPrecoius(){
        try {
            Connection conn = DriverManager.getConnection(url, username, password);
            Statement statement = ((Connection) conn).createStatement();
            ResultSet result = statement.executeQuery("select * from stone where type = 'semi_precious'");
            int cost = 0;
            while(result.next()){
                cost = cost +result.getInt("price");
            }
            System.out.println(result.getInt("Total cost is " + cost + "$"));
            System.out.println("------------------------------");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void toStrinng() {
        try {
            Connection conn = DriverManager.getConnection(url, username, password);
            Statement statement = ((Connection) conn).createStatement();
            ResultSet result = statement.executeQuery("select * from stone");
            while(result.next()) {
                System.out.println(result.getString("type") + " stone "
                        + result.getString("name") + " with price: "
                        + result.getString("price") + " and weight: "
                        + result.getString("weight"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
