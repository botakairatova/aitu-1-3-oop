package kz.aitu.oop.examples.practice4;

import java.sql.*;
import java.util.ArrayList;

public class Aquarium {
   public int size;
   public int width;
   public int height;
   public int length;
   public ArrayList<Fish> fishName;
   public ArrayList<Reptile> reptileName;

    String url = "jdbc:mysql://localhost:3306/bota";
    String username = "root";
    String password = "Botakan2001";


    public Aquarium(int size, int width, int height,
                    int length) {
        this.size = size;
        this.width = width;
        this.height = height;
        this.length = length;

    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public ArrayList<Fish> getFishName() {
        return fishName;
    }

    public void setFishName(ArrayList<Fish> fishName) {
        this.fishName = fishName;
    }

    public ArrayList<Reptile> getReptileName() {
        return reptileName;
    }

    public void setReptileName(ArrayList<Reptile> reptileName) {
        this.reptileName = reptileName;
    }
    public void totalCost(){
        try {
            Connection conn = DriverManager.getConnection(url, username, password);
            Statement statement = ((Connection) conn).createStatement();
            ResultSet result = statement.executeQuery("select * from aquarium");
            int cost = 0;
            while(result.next()){
                cost = cost +result.getInt("price");
            }
            System.out.println(result.getInt("Total cost is " + cost + "$"));
            System.out.println("------------------------------");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
