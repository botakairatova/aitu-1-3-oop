package kz.aitu.oop.examples.practice4;

import java.sql.*;

public class Fish implements Animal{
    public int fin_length;

    public int getFin_length() {
        return fin_length;
    }

    public void setFin_length(int fin_length) {
        this.fin_length = fin_length;
    }



    public String toAction() {
        return "Fish swims!";
    }
}
