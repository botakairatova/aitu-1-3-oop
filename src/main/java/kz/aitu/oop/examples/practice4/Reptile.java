package kz.aitu.oop.examples.practice4;

import java.sql.*;

public class Reptile implements Animal{
    public int tailLength;

    public int getTailLength() {
        return tailLength;
    }

    public void setTailLength(int tailLength) {
        this.tailLength = tailLength;
    }

    @Override
    public String toAction() {
        return "Reptile crawls!";
    }
}
