package kz.aitu.oop.examples.practice4;

public class Animal_factory {
    public static Animal getAnimal(String animalType){
        if("Fish".equalsIgnoreCase(animalType)){
            return new Fish();
        }else if("Reptile".equalsIgnoreCase(animalType)){
            return new Reptile();
        }
        return null;
    }

}
