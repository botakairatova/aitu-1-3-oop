package kz.aitu.oop.examples.practice2;

public class Railway {
	public int railwayID;
	public String name;

	public Railway(int railwayID, String name) {
		this.railwayID = railwayID;
		this.name = name;
	}

	public int getRailwayID() {
		return railwayID;
	}

	public void setRailwayID(int railwayID) {
		this.railwayID = railwayID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String toString(){
	    return " This is railway with ID: " + railwayID + " with name " + name;
    }
}
