package kz.aitu.oop.examples.practice2;

import java.sql.*;
import java.util.ArrayList;

public class Train extends Carriage {
           public int trainID;
           public String trainName;
           public String StationName;

    String url = "jdbc:mysql://localhost:3306/bota";
    String username = "root";
    String password = "Botakan2001";

    public int getTrainID() {
        return trainID;
    }

    public String getTrainName() {
        return trainName;
    }

    public String getStationName() {
        return StationName;
    }

    public Train(String type, int carriageID, int capasity, int trainID, String trainName, String stationName) {
        super(type, carriageID, capasity);
        this.trainID = trainID;
        this.trainName = trainName;
        StationName = stationName;
    }

    public void addCariage(){
        ArrayList<String> carriage = new ArrayList<>();
        try {
            Connection conn = DriverManager.getConnection(url, username, password);
            Statement statement = ((Connection) conn).createStatement();
            ResultSet result = statement.executeQuery("select * from  carriage");
            while (result.next()) {
                carriage.add(result.getString("carriageID"));
            }
        }
        catch(SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteCariage(){
        ArrayList<String> carriage = new ArrayList<>();
        try {
            Connection conn = DriverManager.getConnection(url, username, password);
            Statement statement = ((Connection) conn).createStatement();
            ResultSet result = statement.executeQuery("select * from  carriage");
            while (result.next()) {
                carriage.remove(result.getString("carriageID"));
            }
        }
        catch(SQLException e) {
            e.printStackTrace();
        }
    }
    public void addDriver(){
        ArrayList<String> Driver = new ArrayList<>();
        try {
            Connection conn = DriverManager.getConnection(url, username, password);
            Statement statement = ((Connection) conn).createStatement();
            ResultSet result = statement.executeQuery("select * from  driver");
            while (result.next()) {
                Driver.add(result.getString("driverName"));
            }
        }
        catch(SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteDriver(){
        ArrayList<String> Driver = new ArrayList<>();
        try {
            Connection conn = DriverManager.getConnection(url, username, password);
            Statement statement = ((Connection) conn).createStatement();
            ResultSet result = statement.executeQuery("select * from  driver");
            while (result.next()) {
                Driver.remove(result.getString("driverName"));
            }
        }
        catch(SQLException e) {
            e.printStackTrace();
        }


    }





}
