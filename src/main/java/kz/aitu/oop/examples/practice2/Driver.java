package kz.aitu.oop.examples.practice2;

import java.sql.*;

public class Driver {
    private int driverID;
    private String driverName;
    private String driverSurname;
    private String driverAddress;

    String url = "jdbc:mysql://localhost:3306/bota";
    String username = "root";
    String password = "Botakan2001";

    public int getDriverID() {
        return driverID;
    }

    public void setDriverID(int driverID) {
        this.driverID = driverID;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDriverSurname() {
        return driverSurname;
    }

    public void setDriverSurname(String driverSurname) {
        this.driverSurname = driverSurname;
    }

    public String getDriverAddress() {
        return driverAddress;
    }

    public void setDriverAddress(String driverAddress) {
        this.driverAddress = driverAddress;
    }

    public Driver(int driverID, String driverName, String driverSurname, String driverAddress) {
        this.driverID = driverID;
        this.driverName = driverName;
        this.driverSurname = driverSurname;
        this.driverAddress = driverAddress;
    }

    public static class DriverBuilder {

        private int driverID;
        private String driverName;
        private String driverSurname;
        private String driverAddress;



        public DriverBuilder(int driverID) {
            this.driverID = driverID;
        }

        public DriverBuilder withName(String driverName){
            this.driverName = driverName;
            return this;
        }

        public DriverBuilder withSurname(String driverSurname) {
            this.driverSurname = driverSurname;
            return this;
        }

        public DriverBuilder withAddress(String address){
            this.driverAddress = driverName;
            return this;
        }
        public Driver build(){
            /*
            Driver dr = new Driver();
            dr.driverID = this.driverID;
            dr.driverName = this.driverName;
            dr.driverSurname = this.driverSurname;
            dr.driverAddress = this.driverAddress;
            return dr;
             */
            return new Driver(driverID,driverName,driverSurname,driverSurname);
        }

    }
    private Driver(){}
    public void toString1() {
        try {
            Connection conn = DriverManager.getConnection(url, username, password);
            Statement statement = ((Connection) conn).createStatement();
            ResultSet result = statement.executeQuery("select * from Driver");
            while (result.next()) {
                System.out.println("Driver: " + result.getString("driverName") +
                        "  " + result.getString("driverSurname") + " with ID  "
                        + result.getString("driverID") + " where lives:  " + result.getString("driverAddress"));
                System.out.println("------------------------------");

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }
}
