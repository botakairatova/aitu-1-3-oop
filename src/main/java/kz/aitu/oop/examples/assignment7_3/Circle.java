package kz.aitu.oop.examples.assignment7_3;

public class Circle implements GeometricObject{
    protected double radius;

    public Circle() {
        radius = 1.0;
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public String toString() {
        return "Circle with " + "radius = " + radius;
    }

    @Override
    public double getArea() {
        double area = 3.14 * Math.pow(radius, 2);
        return area;
    }
    @Override
    public double getPerimeter() {
        double perimeter = 2 * 3.14 * radius;
        return perimeter;
    }

}
