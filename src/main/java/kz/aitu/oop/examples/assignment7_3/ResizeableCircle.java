package kz.aitu.oop.examples.assignment7_3;

public class ResizeableCircle extends Circle implements Resizable{

    public ResizeableCircle(double radius) {
        super(radius);
    }

    @Override
    public String toString() {
        return "ResizeableCircle";
    }

    @Override
    public void resize(int percent) {
       double size = (radius *percent)/100;
        System.out.println(size);

    }
}
