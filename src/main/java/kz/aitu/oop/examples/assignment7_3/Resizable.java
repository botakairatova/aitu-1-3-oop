package kz.aitu.oop.examples.assignment7_3;

public interface Resizable {
    public void resize(int percent);
}
