package kz.aitu.oop.examples.assignment7_3;

public interface GeometricObject {
     public double getArea();
     public double getPerimeter();
}
