package kz.aitu.oop.examples.practice3;

import java.sql.*;

public class Employee {
    private int Salary;
    public int officeRoom;
    public  String profession;
    private String name;
    public int empID;

    String url = "jdbc:mysql://localhost:3306/bota";
    String username = "root";
    String password = "Botakan2001";

    public int getSalary() {
        return Salary;
    }

    public void setSalary(int salary) {
        Salary = salary;
    }

    public int getOfficeRoom() {
        return officeRoom;
    }

    public void setOfficeRoom(int officeRoom) {
        this.officeRoom = officeRoom;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getEmpID() {
        return empID;
    }

    public void setEmpID(int empID) {
        this.empID = empID;
    }

    public Employee(int salary, int officeRoom, String profession, String name, int empID) {
        Salary = salary;
        this.officeRoom = officeRoom;
        this.profession = profession;
        this.name = name;
        this.empID = empID;
    }

    public static class EmpBuilder {
        private int Salary;
        public int officeRoom;
        public  String profession;
        private String name;
        public int empID;

   public EmpBuilder(int empID){
       this.empID = empID;
   }

   public EmpBuilder withName(String name){
       this.name = name;
       return this;
   }

   public EmpBuilder withProfession(String profession){
       this.profession = profession;
       return this;
   }

   public EmpBuilder withOfficeRoom(int officeRoom){
       this.officeRoom = officeRoom;
       return this;
   }

   public EmpBuilder withSalary(int Salary){
       this.Salary = Salary;
       return this;
   }

   public Employee build(){
       return new Employee(Salary,officeRoom,profession,name,empID);
   }


    }

    public void toWork(){
        System.out.println("Employee works!");
    }
    public void totalCost(){
        try {
            Connection conn = DriverManager.getConnection(url, username, password);
            Statement statement = ((Connection) conn).createStatement();
            ResultSet result = statement.executeQuery("select * from Employee");
            while(result.next()){
                int cost = 0;
                cost = cost +result.getInt("Salary");
                System.out.println(result.getInt(cost));
                System.out.println("------------------------------");

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void toStrinng() {
        try {
            Connection conn = DriverManager.getConnection(url, username, password);
            Statement statement = ((Connection) conn).createStatement();
            ResultSet result = statement.executeQuery("select * from Employee");
            while(result.next()){
                System.out.println( result.getString("profession") + "  " +result.getString("name") + " with salary  " + result.getString("Salary") + "$ "+
                        " with ID " + result.getString("empID") + " and office room number  "
                        + result.getString("officeRoom"));
                System.out.println("------------------------------");

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
