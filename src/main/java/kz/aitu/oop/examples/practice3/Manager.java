package kz.aitu.oop.examples.practice3;

import java.sql.*;

public class Manager extends Employee {

	public Manager(int salary, int officeRoom, String profession,
				   String name, int empID) {
		super(salary, officeRoom, profession, name, empID);
	}
	@Override
    public void toStrinng(){
        try {
            Connection conn = DriverManager.getConnection(url, username, password);
            Statement statement = ((Connection) conn).createStatement();
            ResultSet result = statement.executeQuery("select * from Employee where empID = 2");
            while(result.next()){
                System.out.println( result.getString("profession") + "  " +result.getString("name") + " with salary  " + result.getString("Salary") + "$ "+
                        " with ID " + result.getString("empID") + " and office room number  "
                        + result.getString("officeRoom"));
                System.out.println("------------------------------");

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void toWork() {
        System.out.println("Manager manages!");
     }
}
