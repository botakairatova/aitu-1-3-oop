package kz.aitu.oop.examples.practice3;

import java.sql.*;

public class IT_specialist extends Employee{
    public String type;

    public IT_specialist(int salary, int officeRoom, String profession,
                         String name, int empID, String type) {
        super(salary, officeRoom, profession, name, empID);
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public void toStrinng(){
        try {
            Connection conn = DriverManager.getConnection(url, username, password);
            Statement statement = ((Connection) conn).createStatement();
            ResultSet result = statement.executeQuery("select * from Employee where empID = 1");
            while(result.next()){
                System.out.println( result.getString("profession") + "  "
                        +result.getString("name") +
                        " with salary  " + result.getString("Salary") + "$ "+
                        " with ID " + result.getString("empID") +
                        " and office room number  "
                        + result.getString("officeRoom"));
                System.out.println("------------------------------");

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void toWork() {
        System.out.println("IT-specialist codes!");
    }
}
