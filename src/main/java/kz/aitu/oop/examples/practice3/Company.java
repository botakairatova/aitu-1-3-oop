package kz.aitu.oop.examples.practice3;

public class Company {
    private int budget;
    public String address;
    private int NumberOfEmployees;
    public int compID;

    public Company(int budget, String address, int numberOfEmployees, int compID) {
        this.budget = budget;
        this.address = address;
        NumberOfEmployees = numberOfEmployees;
        this.compID = compID;
    }

    public int getBudget() {
        return budget;
    }

    public void setBudget(int budget) {
        this.budget = budget;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getNumberOfEmployees() {
        return NumberOfEmployees;
    }

    public void setNumberOfEmployees(int numberOfEmployees) {
        NumberOfEmployees = numberOfEmployees;
    }

    public int getCompID() {
        return compID;
    }

    public void setCompID(int compID) {
        this.compID = compID;
    }

    public void toSttring(){
        System.out.println("A company with budget");
    }
}
