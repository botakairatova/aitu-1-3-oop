package kz.aitu.oop.examples.quiz_week7;

import java.util.Scanner;

public class main {
    public static void main(String[] args) {
        Scanner scanInput = new Scanner(System.in);
        String str =  scanInput.nextLine();

        singleton sin = singleton.getSingleInstance();
        System.out.println("Hello, I am a singleton! Let me say " + str + " to you! ");

    }
}
