package kz.aitu.oop.examples.quiz_week7;

import java.util.Scanner;

public class singleton {
    public String str;

    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }

    private static singleton instance = null;

    public singleton() {

    }

    public static singleton getSingleInstance(){
        if(instance == null) {
            instance = new singleton();
        }
        return instance;
    }
}
