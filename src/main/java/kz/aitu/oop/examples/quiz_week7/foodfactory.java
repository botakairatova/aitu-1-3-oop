package kz.aitu.oop.examples.quiz_week7;

public class foodfactory {
        public static Food getFood(String food) {
           if("pizza".equalsIgnoreCase(food)){
               return new pizza();
           }
           else if("cake".equalsIgnoreCase(food)){
               return new cake();
           }
           return null;
        }
}
