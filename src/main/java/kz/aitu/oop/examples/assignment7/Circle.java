package kz.aitu.oop.examples.assignment7;

public class Circle extends Shape{
    protected double radius;

    public Circle() {
        radius = 1.0;
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public Circle(String color, Boolean filled, double radius) {
        super(color, filled);
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public double getArea() {
        double area = 3.14 * Math.pow(radius, 2);
        return area;
    }

    @Override
    public double getPerimeter() {
        double perimeter = 2 * 3.14 * radius;
        return perimeter;
    }

    @Override
    public String toString() {
        return "A Circle with radius " + radius
                + " which is a subclass of " + super.toString();
    }
}
