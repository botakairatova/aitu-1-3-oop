package kz.aitu.oop.examples.assignment7;

public abstract class Shape {
    protected String color;
    protected Boolean filled;

    public Shape(String color, Boolean filled) {
        this.color = color;
        this.filled = filled;
    }

    public Shape() {
        color = "red";
        filled = true;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Boolean isFilled() {
        return filled;
    }

    public void setFilled(Boolean filled) {
        this.filled = filled;
    }
    abstract public double  getArea();

    abstract public double getPerimeter();

    public String toString(){
            if(filled){
                return "a Shape with color of " + color + " and filled";
            }else{
                return "a Shape with color of " + color + " and not filled";
            }
    }
}

