package kz.aitu.oop.examples.assignment7;

public class Main2 {
    public static void main(String[] args) {
        MovablePoint mp = new MovablePoint(1, 3, 3, 1);
        System.out.println(mp.toString());
        mp.moveUp();
        mp.moveDown();
        mp.moveRigth();
        mp.moveLeft();

        MovableCircle mc = new MovableCircle(2, 3, 1, 4, 2);
        System.out.println(mc.toString());
        mc.moveUp();
        mc.moveDown();
        mc.moveRigth();
        mc.moveLeft();

    }
}
