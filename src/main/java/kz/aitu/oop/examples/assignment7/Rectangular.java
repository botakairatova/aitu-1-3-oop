package kz.aitu.oop.examples.assignment7;

public class Rectangular extends Shape{
    protected double width;
    protected double length;

    public Rectangular() {
        width = 1.0;
        length = 1.0;
    }

    public Rectangular(double width, double length) {
        this.width = width;
        this.length = length;
    }

    public Rectangular(String color, Boolean filled, double width, double length) {
        super(color, filled);
        this.width = width;
        this.length = length;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    @Override
    public double getArea() {
        double area = width * length;
        return area;
    }

    @Override
    public double getPerimeter() {
        double perimeter = 2 * (length + width);
        return perimeter;
    }

    @Override
    public String toString() {
        return  "A Rectangular with length " + length  +" and with width " + width
                + " which is a subclass of " + super.toString();
    }
}
