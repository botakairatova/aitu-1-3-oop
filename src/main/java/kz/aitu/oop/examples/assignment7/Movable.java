package kz.aitu.oop.examples.assignment7;

public interface Movable {
   abstract public void moveUp();
   abstract public void moveDown();
   abstract public void moveLeft();
   abstract public void moveRigth();
}
