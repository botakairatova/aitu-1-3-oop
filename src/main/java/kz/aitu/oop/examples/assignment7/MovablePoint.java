package kz.aitu.oop.examples.assignment7;

import java.sql.SQLOutput;

public class MovablePoint implements Movable {
    int x;
    int y;
    int xSpeed;
    int ySpeed;

    public MovablePoint(int x, int y, int xSpeed, int ySpeed) {
        this.x = x;
        this.y = y;
        this.xSpeed = xSpeed;
        this.ySpeed = ySpeed;
    }

    @Override
    public void moveUp() {
        int up = y+ySpeed;
        System.out.println("MoveUP: " + up);
    }

    @Override
    public void moveDown() {
        int down = y-ySpeed;
        System.out.println("MoveDown: " + down);

    }

    @Override
    public void moveLeft() {
        int left = x-xSpeed;
        System.out.println("MoveLeft: " + left);
    }

    @Override
    public void moveRigth() {
        int rigth = x+xSpeed;
        System.out.println("MoveRigth: " + rigth);
    }

    public String toString() {
        return "MovablePoint " + "x = " + x +
                ", y = " + y +
                ", xSpeed = " + xSpeed +
                ", ySpeed = " + ySpeed;
    }
}
