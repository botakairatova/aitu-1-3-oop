package kz.aitu.oop.examples.assignment7;

public class Square extends Rectangular{
    public Square() {
    }

    public Square(double side) {
        super(side, side);
    }

    public Square(String color, Boolean filled, double side) {
        super(color, filled, side, side);
    }

    public void setSide(double side) {
        super.getLength();
        super.getWidth();
    }

    public double getSide() {
        return super.getWidth();
    }


    @Override
    public void setWidth(double width) {
        super.setWidth(width);
    }

    @Override
    public void setLength(double length) {
        super.setLength(length);
    }

    @Override
    public String toString() {
        return "A Square with side  " + getSide() + " , which is a subclass of " + super.toString();
    }
}
